﻿using UnityEngine;

using System;
using System.Collections;

using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
//[System.Serializable]

public class StateObjectBuffer{
	public Socket workSocket = null;
	public const int BUFFER_SIZE = (921600);
	public byte[] buffer = new byte[BUFFER_SIZE];
	public StringBuilder sb = new StringBuilder();
}

public class UDPClient {
	
	private static Socket _socket;
	private static IPAddress _mCastIP;
	private static int _port;
	private static MulticastOption _mcastOption;
	public static bool _messageReceived = false;
	//[SerializeField]
	private static StateObjectBuffer _stateObject;// = new StateObjectBuffer ();
	
	public static int _countPacket = 0;
	public static int _sizePacket = 0;
	public static string _sizeMessage = string.Empty;
	
	//    private const int BUFFER_SIZE = 1024;
	//    private byte[] buffer = new byte[BUFFER_SIZE];
	//public static string _message = string.Empty;
	
	/*
    public UDPClient(string mcastGroup, string port)
    {

    }*/
	
	public static void CloseSocket()
	{
		//_socket.Close();
	}
	
	public static void StartMulticastGoup(string mcastGroup, string port)
	{
		_mCastIP=IPAddress.Parse(mcastGroup);
		_port = int.Parse(port);
		_socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
		
		//IPAddress localIPAddr = IPAddress.Parse("192.168.1.102");
		//IPAddress localIPAddr = IPAddress.Parse("127.0.0.1");
		IPAddress localIPAddr = IPAddress.Any;
		
		IPEndPoint localEP = new IPEndPoint(localIPAddr,_port);
		_socket.Bind(localEP);
		
		// Define a MulticastOption object specifying the multicast group 
		// address and the local IPAddress.
		// The multicast group address is the same as the address used by the server.
		_mcastOption = new MulticastOption(_mCastIP, localIPAddr);
		
		_socket.SetSocketOption(SocketOptionLevel.IP, 
		                        SocketOptionName.AddMembership, 
		                        _mcastOption);
		
		//recive method
		//_socket.BeginReceive(new AsyncCallback(ReceiveCallback), s);
		//_socket.BeginReceiveFrom ();
		
		Receive (_socket);
		
		Debug.Log ("UDPClient: calling first callback");
		
	}
	
	private static bool Receive(Socket client) 
	{
		try 
		{
			// Create the state object.
			_stateObject = new StateObjectBuffer();
			_stateObject.workSocket = client;
			
			client.BeginReceive( _stateObject.buffer, 0, StateObjectBuffer.BUFFER_SIZE, 0,
			                    new AsyncCallback(ReceiveCallback), _stateObject);
			
		} catch (Exception e) 
		{
			Debug.Log(e.ToString());
			return false;
		}
		
		return true;
	}
	
	private static void ReceiveCallback(IAsyncResult ar)
	{
		try
		{
			//Socket s = (Socket) ar.AsyncState as Socket;
			StateObjectBuffer so = (StateObjectBuffer) ar.AsyncState;
			Socket tmpSocket = so.workSocket;
			
			int receiveState = tmpSocket.EndReceive(ar);
			
			if (receiveState > 0) 
			{
				
				ParsePacket(so.buffer, receiveState);
				tmpSocket.BeginReceive( _stateObject.buffer, 0, StateObjectBuffer.BUFFER_SIZE, 0,
				                       new AsyncCallback(ReceiveCallback), _stateObject);
				
			} else {
				
				tmpSocket.Close();
			}
			
		} catch (Exception e) 
		{
			Debug.LogError(e.ToString());
		}
		
	}
	
	private static void ParsePacket(Byte[] b, int size)
	{
		
		//_message = Encoding.ASCII.GetString(so.buffer, 0, receiveState);
		_messageReceived = true;
		_sizeMessage = Encoding.ASCII.GetString(b, 0, 4);
		
		_sizePacket = size;
		_countPacket++;
		Debug.Log("UDPClient: RecieveMessage: " + _countPacket + ",off " + _sizeMessage + ",off " + _sizePacket);
	}
	
	
	private static void ReceiveBroadcastMessages() 
	{
		bool done = false;
		byte[] bytes = new Byte[100];
		IPEndPoint groupEP = new IPEndPoint(_mCastIP, _port);
		EndPoint remoteEP = (EndPoint) new IPEndPoint(IPAddress.Any,0);
	}
	
	public  static void GetMulticastOptionProperties()
	{
		Debug.Log("UDPClient: multicast group ip: " + _mcastOption.Group.ToString());
		Debug.Log("UDPClient: multicast local ip: " + _mcastOption.LocalAddress.ToString());
		Debug.Log("UDPClient: interface: " + _mcastOption.InterfaceIndex.ToString());
	}
	
}


