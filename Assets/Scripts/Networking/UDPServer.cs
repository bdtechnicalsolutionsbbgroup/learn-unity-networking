using UnityEngine;

using System;
using System.Collections;

using System.Net;
using System.Net.Sockets;
using System.Text;

//[System.Serializable]
public class UDPServer {

	private static Socket _socket;
	private static IPAddress _mCastIP;
	private static int _port;
	private static MulticastOption _mcastOption;

	//[SerializeField]
	public string _serverIP;

	/*
	public UDPServer(string mcastGroup, string port, string ttl)
	{
		//Console.WriteLine("MCAST Send on Group: {0} Port: {1} TTL: {2}",mcastGroup,port,ttl);
	}

	~UDPServer()  // destructor
	{
		// cleanup statements...
		_socket.Close();
	}
*/

	public static void CloseSocket()
	{
		_socket.Close();
	}

	public static void StartMulticastGroup(string mcastGroup, string port, string ttl)
	{
		try 
		{
			
			_mCastIP=IPAddress.Parse(mcastGroup);
			_socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
			
			//192.168.1.102
			
			_port = int.Parse(port);
			//IPEndPoint ipep=new IPEndPoint(_mCastIP, _port);
			
			//IPAddress localIPAddr = IPAddress.Parse("192.168.1.102");
			//IPAddress localIPAddr = IPAddress.Parse("127.0.0.1");
			IPAddress localIPAddr = IPAddress.Any;
			
			IPEndPoint IPlocal = new IPEndPoint(localIPAddr, _port);
			_socket.Bind(IPlocal);
			
			//options
			_mcastOption = new MulticastOption(_mCastIP, localIPAddr);
			//mcastOption = new MulticastOption(_mCastIP);
			
			_socket.SetSocketOption(SocketOptionLevel.IP, 
			                        SocketOptionName.AddMembership, 
			                        _mcastOption);
			
			//_socket.SetSocketOption(SocketOptionLevel.IP, 
			//                  SocketOptionName.MulticastTimeToLive, int.Parse(ttl));
			
			//_socket.Connect(ipep);
			
			MulticastMessage("lalala");
			//_socket.BeginReceive();
			
		} 
		catch(System.Exception e) 
		{ 
			Debug.LogError(e.Message);
			//Console.Error.WriteLine(e.Message); 
		}

	}

	public static void MulticastData(byte[] data, int offset, int size)
	{
		IPEndPoint endPoint;

		try
		{
			endPoint = new IPEndPoint(_mCastIP,_port);
			_socket.SendTo(data, offset, size, SocketFlags.None, endPoint);
		}
		catch (System.Exception e)
		{
			Debug.LogError(e.Message);
		}
		
	}

	public static void MulticastData(byte[] data)
	{
		IPEndPoint endPoint;
		//Debug.Log ("data size: " + data.Length);

		try
		{
			//Send multicast packets to the listener.
			endPoint = new IPEndPoint(_mCastIP,_port);
			_socket.SendTo(data, endPoint);
		}
		catch (System.Exception e)
		{
			Debug.LogError(e.Message);
			//Console.WriteLine("\n" + e.ToString());
		}
		
	}

	public static void MulticastMessage(string message)
	{
		if (message.Length == 0)
			return;

		byte[] sendMessage = ASCIIEncoding.ASCII.GetBytes(message);

		Debug.Log ("size: " + sendMessage.Length);

		MulticastData (sendMessage);
	}

	/*
	public void MulticastMessageString(string message)
	{
		IPEndPoint endPoint;
		
		try
		{
			//Send multicast packets to the listener.
			endPoint = new IPEndPoint(_mCastIP,_port);
			byte[] sendMessage = ASCIIEncoding.ASCII.GetBytes(message);
			_socket.SendTo(sendMessage, endPoint);

			//_socket.Send(sendMessage,sendMessage.Length,SocketFlags.None);

			//Console.WriteLine("Multicast data sent.....");
		}
		catch (System.Exception e)
		{
			Debug.LogError(e.Message);
			//Console.WriteLine("\n" + e.ToString());
		}

	}
*/
	
}
