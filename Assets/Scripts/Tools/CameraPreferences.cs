﻿using UnityEngine;
using System.Collections;

public class CameraPreferences : MonoBehaviour {

	public bool _openBeforeLaunch = false;
	// Use this for initialization
	void Start () 
	{
		if(_openBeforeLaunch)
			OpenCameraPreferences ();
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	public void OpenCameraPreferences()
	{
		#if UNITY_ANDROID
		if (Application.platform == RuntimePlatform.Android) {
			using (AndroidJavaClass jc = new AndroidJavaClass("com.unity3d.player.UnityPlayer")) {
				AndroidJavaObject jo = jc.GetStatic<AndroidJavaObject>("currentActivity");
				jo.Call("launchPreferencesActivity");
			}
		}
		#endif

	}
}
